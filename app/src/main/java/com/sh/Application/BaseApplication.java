package com.sh.Application;

import android.app.Application;
import android.content.Context;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.stetho.Stetho;
import com.sh.struts.LoginInfo;

public class BaseApplication extends Application {
    private static Context context;

    private static LoginInfo loginInfo;

    @Override
    public void onCreate() {
        super.onCreate();
      /*  JMessageClient.setDebugMode(true);
        JMessageClient.init(this,true);
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());

            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());
        }*/
        Fresco.initialize(this);
        context = this.getApplicationContext();
        Stetho.initializeWithDefaults(this);
    }

    public static Context getInstance() {
        return context;
    }

    public static LoginInfo getLoginInfo() {
        return loginInfo;
    }

    public static void setLoginInfo(LoginInfo loginInfo) {
        BaseApplication.loginInfo = loginInfo;
    }
}

