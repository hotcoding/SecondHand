package com.sh.struts;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class ProductWrapper implements Parcelable {
    @SerializedName("title")
    @Expose
    private String title;
    protected ProductWrapper(Parcel in) {
        title = in.readString();
    }

    public static final Creator<ProductWrapper> CREATOR = new Creator<ProductWrapper>() {
        @Override
        public ProductWrapper createFromParcel(Parcel in) {
            return new ProductWrapper(in);
        }

        @Override
        public ProductWrapper[] newArray(int size) {
            return new ProductWrapper[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
    }
}
