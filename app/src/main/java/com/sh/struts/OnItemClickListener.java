package com.sh.struts;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View view);
    void onItemLongClick(View view);
}
