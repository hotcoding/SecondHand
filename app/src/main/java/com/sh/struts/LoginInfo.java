package com.sh.struts;

import lombok.Data;

@Data
public class LoginInfo {
    Integer userId;
    String username;
    String userImgUrl;
}
