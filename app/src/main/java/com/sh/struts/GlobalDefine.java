package com.sh.struts;

import java.util.ArrayList;
import java.util.List;

public class GlobalDefine {
    //dev
//    public static final String BACKGROUND_MANAGER_DOMAIN = "http://192.168.31.96:8070";
//    public static final String USER_MANAGER_DOMAIN = "http://192.168.31.96:8090";

    //publish
    public static final String BACKGROUND_MANAGER_DOMAIN = "http://62.234.76.191:8070";
    public static final String USER_MANAGER_DOMAIN = "http://62.234.76.191:8090";

    public static final Integer SP_USER_COUNT_MAX = 5;
    public static final String QINIU_DOMAIN = "http://cdn.hotcoding.top";

    public enum USER_TYPE {
        REGULAR_USER,
        VISITOR,
        MANAGER
    }

    public enum SORT_CONDITION {
        recommend("推荐"), price_desc("价格从高到低"), price_asc("价格从低到高"), distance_near("离我最近");
        private String cName;

        public String getCName() {
            return this.cName;
        }

        private SORT_CONDITION(String name) {
            this.cName = name;
        }

        public static SORT_CONDITION getObjectByCname(String CName) {
            for (SORT_CONDITION condition : SORT_CONDITION.values()) {
                if (condition.cName.equals(CName)) {
                    return condition;
                }
            }
            return null;
        }

        public static SORT_CONDITION getObjectByName(String name) {
            for (SORT_CONDITION condition : SORT_CONDITION.values()) {
                if (condition.name().equals(name)) {
                    return condition;
                }
            }
            return null;
        }

        public static List<String> getCNames() {
            List<String> CNames = new ArrayList<>();
            for (SORT_CONDITION condition : SORT_CONDITION.values()) {
                CNames.add(condition.getCName());
            }
            return CNames;
        }
    }

    public enum DISTANCE {
        no_limit("不限", -1), less_1000("一千米", 1000), less_2000("两千米", 2000), less_5000("五千米", 5000);
        private String cName;
        private Integer distance;

        public String getCName() {
            return this.cName;
        }

        public Integer getDistance(){
            return this.distance;
        }

        private DISTANCE(String name, Integer distance) {
            this.cName = name;
            this.distance = distance;
        }

        public static DISTANCE getObjectByCname(String CName) {
            for (DISTANCE distance : DISTANCE.values()) {
                if (distance.cName.equals(CName)) {
                    return distance;
                }
            }
            return null;
        }

        public static DISTANCE getObjectByName(String name) {
            for (DISTANCE distance : DISTANCE.values()) {
                if (distance.name().equals(name)) {
                    return distance;
                }
            }
            return null;
        }

        public static List<String> getCNames() {
            List<String> CNames = new ArrayList<>();
            for (DISTANCE distance : DISTANCE.values()) {
                CNames.add(distance.getCName());
            }
            return CNames;
        }
    }

    public enum GENRE_TYPE {
        phone("手机"),beauty_makeup("美妆"),car("汽车"),culture("文化用品"),digital("数码3C"),
        dress("服饰"),ele("家用电器"),game("虚拟物品"),daily("日用品");
        private String genre_name;
        GENRE_TYPE(String genre_name){
            this.genre_name = genre_name;
        }

        public String getGenreName(){
            return this.genre_name;
        }
        public static GENRE_TYPE getObject(String genre_name){
            for(GENRE_TYPE genre_type : GENRE_TYPE.values()){
                if(genre_type.name().equals(genre_name)){
                    return genre_type;
                }
            }
            return null;
        }
    }
}
