package com.sh.struts;

import com.sh.utils.CommonStringUtils;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class ProductData implements Serializable {
    public String img;
    public String title;
    public double price;
    public String username;
    public Integer userId;
    public String userImg;
    public Integer productId;
    public double buyPrice;
    public double expressFee;
    public boolean isCollected;
    public boolean isValid;

    public ProductData(Map<String, String> productInfo) {
        this.img = CommonStringUtils.safeString(productInfo.get("show_img"));
        this.title = CommonStringUtils.safeString(productInfo.get("title"));
        this.price = CommonStringUtils.safeDouble(productInfo.get("price"));
        this.username = CommonStringUtils.safeString(productInfo.get("username"));
        this.userImg = CommonStringUtils.safeString(productInfo.get("user_img"));
        this.userId = CommonStringUtils.safeInteger(productInfo.get("user_id"));
        this.productId = CommonStringUtils.safeInteger(productInfo.get("pro_id"));
        this.isCollected = CommonStringUtils.safeInteger(productInfo.get("is_collected")) == 1 ? true : false;
        this.buyPrice = CommonStringUtils.safeDouble(productInfo.get("buy_price"));
        this.expressFee = CommonStringUtils.safeDouble(productInfo.get("express_fee"));
        this.isValid = "insale".equals(productInfo.get("status"));
    }
}
