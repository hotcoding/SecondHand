package com.sh.struts;

import com.sh.utils.CommonStringUtils;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class DeliveryAddressData implements Serializable {
    public Integer id;
    public Integer userId;
    public String name;
    public String mobile;
    public String province;
    public String city;
    public String schoolName;
    public String fullAddress;
    public String postCode;
    public Boolean isDefault;

    public DeliveryAddressData() {
    }

    public DeliveryAddressData(Map<String, String> map) {
        this.id = CommonStringUtils.safeInteger(map.get("id"));
        this.userId = CommonStringUtils.safeInteger(map.get("user_id"));
        this.name = CommonStringUtils.safeString(map.get("name"));
        this.mobile = CommonStringUtils.safeString(map.get("mobile"));
        this.province = CommonStringUtils.safeString(map.get("province"));
        this.city = CommonStringUtils.safeString(map.get("city"));
        this.schoolName = CommonStringUtils.safeString(map.get("school_name"));
        this.fullAddress = CommonStringUtils.safeString(map.get("full_address"));
        this.postCode = CommonStringUtils.safeString(map.get("post_code"));
        this.isDefault = Boolean.valueOf(map.get("is_default"));
    }

    public String getRealFullAddress(){
        String address = String.format("%s%s %s %s",province,city,schoolName,fullAddress);
        return address;
    }
}
