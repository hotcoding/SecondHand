package com.sh.struts;

import com.sh.utils.CommonStringUtils;
import com.sh.utils.DateUtils;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

@Data
public class UserInfo implements Serializable {
    public Integer userId;
    public String username;
    public Date birthDay;
    public String imgPath;
    public String province;
    public String gender;
    public int age;
    public String mobile;
    public String brief;

    public UserInfo(){}

    public UserInfo(Map<String,String> map){
        if(map == null){
            return;
        }
        this.userId = CommonStringUtils.safeInteger(map.get("user_id"));
        this.username = map.get("username");
        this.imgPath = map.get("img_path");
        this.province = map.get("province");
        this.gender = map.get("gender");
        this.birthDay = DateUtils.StrToDate(map.get("birthday"),"yyyy-MM-dd");
        this.age = DateUtils.getAgeByBirth(this.birthDay);
        this.mobile = map.get("mobile");
        this.brief = map.get("brief");
    }
}
