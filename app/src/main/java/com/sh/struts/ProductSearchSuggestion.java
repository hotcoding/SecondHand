package com.sh.struts;

import android.annotation.SuppressLint;
import android.os.Parcel;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import lombok.Data;

@Data
@SuppressLint("ParcelCreator")
public class ProductSearchSuggestion implements SearchSuggestion {
    private String title;
    private boolean isHistory = false;

    public ProductSearchSuggestion(String suggestion) {
        this.title = suggestion.toLowerCase();
        this.isHistory = true;
    }

    public ProductSearchSuggestion(Parcel source) {
        this.title = source.readString();
        this.isHistory = source.readInt() != 0;
    }

    @Override
    public String getBody() {
        return this.title;
    }

    public static final Creator<ProductSearchSuggestion> CREATOR = new Creator<ProductSearchSuggestion>() {
        @Override
        public ProductSearchSuggestion createFromParcel(Parcel in) {
            return new ProductSearchSuggestion(in);
        }

        @Override
        public ProductSearchSuggestion[] newArray(int size) {
            return new ProductSearchSuggestion[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeInt(isHistory ? 1 : 0);

    }
}
