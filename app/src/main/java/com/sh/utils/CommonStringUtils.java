package com.sh.utils;

import com.qiniu.android.utils.StringUtils;

import java.util.List;

public class CommonStringUtils {
    /**
     * 将字符串转为double
     * @param str
     * @return
     */
    public static Double safeDouble(String str){
        if(StringUtils.isNullOrEmpty(str)){
            return 0.00;
        }
        double res = 0.00;
        try {
            res = Double.valueOf(str);
        }catch (Exception e){
        }
        return res;
    }

    /**
     * 字符串转为Integer
     * @param str
     * @return
     */
    public static <E> Integer safeInteger(E str){
        String temp = CommonStringUtils.safeString(str);
        if(StringUtils.isNullOrEmpty(temp)){
            return 0;
        }
        int res = 0;
        try {
            res = Integer.valueOf(temp);
        }catch (Exception e){
        }
        return res;
    }

    public static String safeString(Object obj) {
        if (obj == null) {
            return "";
        }
        return obj.toString();
    }

    public static  <E> String join(List<E> list, String str) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Object object : list) {
            stringBuilder.append(CommonStringUtils.safeString(object)+str);
        }
        int size = stringBuilder.length();
        return size > 0 ? stringBuilder.substring(0,size-1) : "";
    }
}
