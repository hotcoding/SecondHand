package com.sh.utils;

import com.qiniu.android.utils.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    public static String DateToStr(Date date, String format_pattern){
        if(date == null){
            return "";
        }
        SimpleDateFormat sp = new SimpleDateFormat(format_pattern);
        String str = sp.format(date);
        return str;
    }

    public static Date StrToDate(String str,String format_pattern){
        if(StringUtils.isNullOrEmpty(str)){
            return null;
        }
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat(format_pattern);
        Date date = null;
        try {
            date=simpleDateFormat.parse(str);
        } catch(ParseException px) {
            px.printStackTrace();
        }
        return date;
    }

    public static int getAgeByBirth(Date birthday) {
        if(birthday == null){
            return  -1;
        }
        int age = 0;
        try {
            Calendar now = Calendar.getInstance();
            now.setTime(new Date());// 当前时间

            Calendar birth = Calendar.getInstance();
            birth.setTime(birthday);

            if (birth.after(now)) {//如果传入的时间，在当前时间的后面，返回0岁
                age = -1;
            } else {
                age = now.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
                if (now.get(Calendar.DAY_OF_YEAR) > birth.get(Calendar.DAY_OF_YEAR)) {
                    age += 1;
                }
            }
            return age;
        } catch (Exception e) {//兼容性更强,异常后返回数据
            return -1;
        }
    }
}
