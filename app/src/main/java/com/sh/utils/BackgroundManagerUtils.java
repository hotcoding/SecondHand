package com.sh.utils;

import android.util.Log;
import com.google.gson.Gson;
import com.qiniu.android.utils.StringUtils;
import com.sh.utils.Http.OkHttpUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import static com.sh.struts.GlobalDefine.BACKGROUND_MANAGER_DOMAIN;

public class BackgroundManagerUtils {
    private static final String TAG = "BackgroundManagerUtils";

    private static final String GET_UPLOADTOKEN = "%s/get_upload_token/";
    private static final String ADD_PRODUCT = "%s/app/add_product/";
    private static final String GET_PRODUCTS_INFOS = "%s/app/get_products_info/";
    private static final String GET_DELIVERY_ADDRESS = "%s/app/get_delivery_addresses/";
    private static final String GET_DEFAULT_DELIVERY_ADDRESS = "%s/app/get_default_delivery_addresses/";
    private static final String CREATE_ORDER = "%s/app/create_ortder/";
    private static final String GET_PRODUCT_BY_ID = "%s/app/get_product_by_id/";
    private static final String REFRESH_PRODUCTS_VIEW = "%s/app/refresh_products_view/";

    /**
     * 获取简单上传凭证
     *
     * @return
     */
    public static String getUploadToken() {
        String url = String.format(GET_UPLOADTOKEN, BACKGROUND_MANAGER_DOMAIN);
        final CountDownLatch latch = new CountDownLatch(1);
        final String[] uploadToken = {""};
        OkHttpUtils.get(url, new OkHttpUtils.ResultCallback<String>() {
            @Override
            public void onSuccess(String response) {
                uploadToken[0] = response;
                latch.countDown();
            }

            @Override
            public void onFailure(Exception e) {
                latch.countDown();
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
        }
        return uploadToken[0];
    }

    public static String addProduct(List<OkHttpUtils.Param> paramList) {
        String url = String.format(ADD_PRODUCT, BACKGROUND_MANAGER_DOMAIN);
        final CountDownLatch latch = new CountDownLatch(1);
        final String[] result = {""};
        OkHttpUtils.post(url, new OkHttpUtils.ResultCallback<String>() {
            @Override
            public void onSuccess(String response) {
                result[0] = response;
                latch.countDown();
            }

            @Override
            public void onFailure(Exception e) {
                latch.countDown();
            }
        }, paramList);
        try {
            latch.await();
        } catch (InterruptedException e) {
        }
        return result[0];
    }

    public static List<Map<String, String>> getProducts(String product_ids, Integer count, Integer user_id, String genre) {
        String url = String.format(GET_PRODUCTS_INFOS, BACKGROUND_MANAGER_DOMAIN);
        List<OkHttpUtils.Param> params = new ArrayList<>();
        if (!StringUtils.isNullOrEmpty(product_ids)) {
            params.add(new OkHttpUtils.Param("poduct_ids", product_ids));
        }
        if (user_id != null && user_id != 0) {
            params.add(new OkHttpUtils.Param("user_id", CommonStringUtils.safeString(user_id)));
        }
        if (!StringUtils.isNullOrEmpty(genre)) {
            params.add(new OkHttpUtils.Param("genre", genre));
        }
        params.add(new OkHttpUtils.Param("count", CommonStringUtils.safeString(count)));
        final CountDownLatch latch = new CountDownLatch(1);
        final String[] result = {""};
        OkHttpUtils.post(url, new OkHttpUtils.ResultCallback<String>() {
            @Override
            public void onSuccess(String response) {
                result[0] = response;
                latch.countDown();
            }

            @Override
            public void onFailure(Exception e) {
                latch.countDown();
            }
        }, params);
        try {
            latch.await();
        } catch (InterruptedException e) {
        }
        List<Map<String, String>> maps = new ArrayList<>();
        if (!StringUtils.isNullOrEmpty(result[0])) {
            try {
                maps = new Gson().fromJson(result[0], List.class);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return maps;
    }

    public static List<Map<String, String>> getProducts(List<OkHttpUtils.Param> params) {
        String url = String.format(GET_PRODUCTS_INFOS, BACKGROUND_MANAGER_DOMAIN);
        final CountDownLatch latch = new CountDownLatch(1);
        final String[] result = {""};
        OkHttpUtils.post(url, new OkHttpUtils.ResultCallback<String>() {
            @Override
            public void onSuccess(String response) {
                result[0] = response;
                latch.countDown();
            }

            @Override
            public void onFailure(Exception e) {
                latch.countDown();
            }
        }, params);
        try {
            latch.await();
        } catch (InterruptedException e) {
        }
        List<Map<String, String>> maps = new ArrayList<>();
        if (!StringUtils.isNullOrEmpty(result[0])) {
            try {
                maps = new Gson().fromJson(result[0], List.class);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return maps;
    }

    public static List<Map<String, String>> refreshProducts(Integer userId) {
        String url = String.format(REFRESH_PRODUCTS_VIEW, BACKGROUND_MANAGER_DOMAIN);
        final CountDownLatch latch = new CountDownLatch(1);
        final String[] result = {""};
        List<OkHttpUtils.Param> params = new ArrayList<>();
        params.add(new OkHttpUtils.Param("user_id",CommonStringUtils.safeString(userId)));
        OkHttpUtils.post(url, new OkHttpUtils.ResultCallback<String>() {
            @Override
            public void onSuccess(String response) {
                result[0] = response;
                latch.countDown();
            }

            @Override
            public void onFailure(Exception e) {
                latch.countDown();
            }
        }, params);
        try {
            latch.await();
        } catch (InterruptedException e) {
        }
        List<Map<String, String>> maps = new ArrayList<>();
        if (!StringUtils.isNullOrEmpty(result[0])) {
            try {
                maps = new Gson().fromJson(result[0], List.class);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return maps;
    }


    public static List<Map<String, String>> getDeliveryAddress(Integer user_id) {
        String url = String.format(GET_DELIVERY_ADDRESS, BACKGROUND_MANAGER_DOMAIN);
        List<OkHttpUtils.Param> params = new ArrayList<>();
        params.add(new OkHttpUtils.Param("user_id", CommonStringUtils.safeString(user_id)));
        final CountDownLatch latch = new CountDownLatch(1);
        final String[] result = {""};
        OkHttpUtils.post(url, new OkHttpUtils.ResultCallback<String>() {
            @Override
            public void onSuccess(String response) {
                result[0] = response;
                latch.countDown();
            }

            @Override
            public void onFailure(Exception e) {
                latch.countDown();
            }
        }, params);
        try {
            latch.await();
        } catch (InterruptedException e) {
        }
        List<Map<String, String>> maps = new ArrayList<>();
        if (!StringUtils.isNullOrEmpty(result[0])) {
            try {
                maps = new Gson().fromJson(result[0], List.class);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return maps;
    }

    public static Map<String, String> getDefaultDeliveryAddress(Integer user_id) {
        String url = String.format(GET_DEFAULT_DELIVERY_ADDRESS, BACKGROUND_MANAGER_DOMAIN);
        List<OkHttpUtils.Param> params = new ArrayList<>();
        params.add(new OkHttpUtils.Param("user_id", CommonStringUtils.safeString(user_id)));
        final CountDownLatch latch = new CountDownLatch(1);
        final String[] result = {""};
        OkHttpUtils.post(url, new OkHttpUtils.ResultCallback<String>() {
            @Override
            public void onSuccess(String response) {
                result[0] = response;
                latch.countDown();
            }

            @Override
            public void onFailure(Exception e) {
                latch.countDown();
            }
        }, params);
        try {
            latch.await();
        } catch (InterruptedException e) {
        }
        Map<String, String> map = new HashMap<>();
        if (!StringUtils.isNullOrEmpty(result[0])) {
            try {
                map = new Gson().fromJson(result[0], Map.class);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return map;
    }

    public static Map<String, String> createOrder(Integer user_id, Integer product_id, Integer delivery_address_id) {
        String url = String.format(CREATE_ORDER, BACKGROUND_MANAGER_DOMAIN);
        List<OkHttpUtils.Param> params = new ArrayList<>();
        params.add(new OkHttpUtils.Param("user_id", CommonStringUtils.safeString(user_id)));
        params.add(new OkHttpUtils.Param("product_id", CommonStringUtils.safeString(product_id)));
        params.add(new OkHttpUtils.Param("delivery_address_id", CommonStringUtils.safeString(delivery_address_id)));
        final CountDownLatch latch = new CountDownLatch(1);
        final String[] result = {""};
        OkHttpUtils.post(url, new OkHttpUtils.ResultCallback<String>() {
            @Override
            public void onSuccess(String response) {
                result[0] = response;
                latch.countDown();
            }

            @Override
            public void onFailure(Exception e) {
                latch.countDown();
            }
        }, params);
        try {
            latch.await();
        } catch (InterruptedException e) {
        }
        Map<String, String> map = new HashMap<>();
        if (!StringUtils.isNullOrEmpty(result[0])) {
            try {
                map = new Gson().fromJson(result[0], Map.class);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return map;
    }

    public static Map<String,String> getProductById(Integer user_id,Integer product_id){
        String url = String.format(GET_PRODUCT_BY_ID, BACKGROUND_MANAGER_DOMAIN);
        List<OkHttpUtils.Param> params = new ArrayList<>();
        params.add(new OkHttpUtils.Param("user_id", CommonStringUtils.safeString(user_id)));
        params.add(new OkHttpUtils.Param("product_id", CommonStringUtils.safeString(product_id)));
        final CountDownLatch latch = new CountDownLatch(1);
        final String[] result = {""};
        OkHttpUtils.post(url, new OkHttpUtils.ResultCallback<String>() {
            @Override
            public void onSuccess(String response) {
                result[0] = response;
                latch.countDown();
            }

            @Override
            public void onFailure(Exception e) {
                latch.countDown();
            }
        }, params);
        try {
            latch.await();
        } catch (InterruptedException e) {
        }
        Map<String, String> map = new HashMap<>();
        if (!StringUtils.isNullOrEmpty(result[0])) {
            try {
                map = new Gson().fromJson(result[0], Map.class);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
        return map;
    }
}
