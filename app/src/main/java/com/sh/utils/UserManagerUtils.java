package com.sh.utils;

import com.google.gson.Gson;
import com.sh.utils.Http.OkHttpUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import static com.sh.struts.GlobalDefine.USER_MANAGER_DOMAIN;

public class UserManagerUtils {
    private static final String USER_LOGIN_URL = "%s/user_login_verify/";
    private static final String USER_SIGNUP_URL = "%s/user_register/";
    private static final String GET_USER_INFO_BY_ID = "%s/background/get_user_info_by_id/?user_id=%s";
    /**
     * 登录处理器
     *
     * @param username
     * @param password
     * @return
     */
    public static Map<String, String> userLoginVerify(String username, String password) {
        String url = String.format(USER_LOGIN_URL, USER_MANAGER_DOMAIN);
        List<OkHttpUtils.Param> postParam = new ArrayList<>();
        postParam.add(new OkHttpUtils.Param("username", username));
        postParam.add(new OkHttpUtils.Param("password", password));
        final Map<String, String>[] resMap = new Map[]{new HashMap<>()};
        final CountDownLatch latch = new CountDownLatch(1);
        OkHttpUtils.post(url, new OkHttpUtils.ResultCallback<String>() {
            @Override
            public void onSuccess(String response) {
                resMap[0] = new Gson().fromJson(response, Map.class);
                latch.countDown();
            }

            @Override
            public void onFailure(Exception e) {
                resMap[0].put("status", "error");
                resMap[0].put("msg", "server error");//服务器错误
                latch.countDown();
            }
        }, postParam);
        try {
            latch.await();
        } catch (InterruptedException e) {
        }
        return resMap[0];
    }

    public static Map<String,String> userSignUp(List<OkHttpUtils.Param> postParam){
        String url = String.format(USER_SIGNUP_URL,USER_MANAGER_DOMAIN);
        final Map<String, String>[] resMap = new Map[]{new HashMap<>()};
        final CountDownLatch latch = new CountDownLatch(1);
        OkHttpUtils.post(url, new OkHttpUtils.ResultCallback<String>() {
            @Override
            public void onSuccess(String response) {
                resMap[0] = new Gson().fromJson(response, Map.class);
                latch.countDown();
            }

            @Override
            public void onFailure(Exception e) {
                resMap[0].put("status", "error");
                resMap[0].put("msg", "server error");//服务器错误
                latch.countDown();
            }
        }, postParam);
        try {
            latch.await();
        } catch (InterruptedException e) {
        }
        return resMap[0];
    }

    public static Map<String,String> getUserInfoById(Integer user_id){
        String url = String.format(GET_USER_INFO_BY_ID,USER_MANAGER_DOMAIN,user_id);
        final Map<String, String>[] resMap = new Map[]{new HashMap<>()};
        final CountDownLatch latch = new CountDownLatch(1);
        OkHttpUtils.get(url, new OkHttpUtils.ResultCallback<String>() {
            @Override
            public void onSuccess(String response) {
                resMap[0] = new Gson().fromJson(response, Map.class);
                latch.countDown();
            }

            @Override
            public void onFailure(Exception e) {
                resMap[0].put("status", "error");
                resMap[0].put("msg", "server error");//服务器错误
                latch.countDown();
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
        }
        return resMap[0];
    }
}
