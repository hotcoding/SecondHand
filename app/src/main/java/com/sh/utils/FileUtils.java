package com.sh.utils;

import com.qiniu.android.utils.StringUtils;

public class FileUtils {
    public static String getFileExt(String fileName){
        String ext = "";
        if(StringUtils.isNullOrEmpty(fileName)){
            return ext;
        }
        ext = fileName.substring(fileName.lastIndexOf(".") + 1);
        return ext;
    }
}
