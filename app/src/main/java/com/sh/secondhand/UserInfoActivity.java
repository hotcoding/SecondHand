package com.sh.secondhand;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.facebook.drawee.view.SimpleDraweeView;
import com.qiniu.android.utils.StringUtils;
import com.sh.Application.BaseApplication;
import com.sh.struts.UserInfo;
import com.sh.utils.CommonStringUtils;
import com.sh.utils.DateUtils;
import com.sh.utils.UserManagerUtils;
import com.sh.utils.Utils;

import java.util.Map;

public class UserInfoActivity extends AppCompatActivity {
    private static final Integer EDIT_INFO_REQUESTCODE = 1800;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.uid_text)
    TextView uidText;
    @BindView(R.id.user_info_text)
    TextView userInfoText;
    @BindView(R.id.address_info_text)
    TextView addressInfoText;
    @BindView(R.id.username_text)
    TextView usernameText;
    @BindView(R.id.edit_btn)
    Button edit;
    @BindView(R.id.brief_text)
    TextView briefText;

    @BindView(R.id.user_img)
    SimpleDraweeView simpleDraweeView;

    private Integer tarUserId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        ButterKnife.bind(this);
        tarUserId = getIntent().getIntExtra("tar_user_id",0);
        if(tarUserId != BaseApplication.getLoginInfo().getUserId()){
            edit.setVisibility(View.GONE);
        }else{
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(UserInfoActivity.this,ModifyUserInfoActivity.class);
                    startActivityForResult(intent,EDIT_INFO_REQUESTCODE);
                }
            });
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        new UserInfoAsync().execute((Void)null);
    }

    public class UserInfoAsync extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            Integer userId;
            if(Utils.checkLogin()) {
                userId = BaseApplication.getLoginInfo().getUserId();
                Map<String, String> map = UserManagerUtils.getUserInfoById(userId);
                UserInfo userInfo = new UserInfo(map);
                runOnUiThread(() -> {
                    if(!StringUtils.isNullOrEmpty(userInfo.imgPath)) {
                        Log.i("image","image； "+userInfo.imgPath);
                        Uri showImgUri = Uri.parse(userInfo.imgPath);
                        simpleDraweeView.setImageURI(showImgUri);
                    }
                    usernameText.setText(userInfo.username);
                    uidText.setText(userId.toString());
                    String userInfoStr = userInfo.gender;
                    if(userInfo.age < 0){
                        userInfoStr += " 年龄未知";
                    }else{
                        userInfoStr += String.format(" %s岁",userInfo.age);
                    }
                    if(userInfo.birthDay != null){
                        userInfoStr += String.format(" 生日： %s", DateUtils.DateToStr(userInfo.birthDay,"yyyy-MM-dd"));
                    }
                    userInfoText.setText(userInfoStr);
                    addressInfoText.setText(StringUtils.isNullOrEmpty(userInfo.province) ? "未知" : userInfo.province);
                    briefText.setText(CommonStringUtils.safeString(userInfo.brief));
                });
            }else{
            }
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == EDIT_INFO_REQUESTCODE) {
                if ("refresh".equals(data.getStringExtra("operation"))) {
                    new UserInfoAsync().execute((Void)null);
                }
            }
        }
    }
}
