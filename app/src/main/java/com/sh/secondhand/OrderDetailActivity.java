package com.sh.secondhand;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.sh.Application.BaseApplication;
import com.sh.struts.GlobalDefine;
import com.sh.struts.ProductData;
import com.sh.utils.BackgroundManagerUtils;
import com.sh.utils.CommonStringUtils;
import com.sh.utils.Utils;

import java.util.Map;

public class OrderDetailActivity extends AppCompatActivity {
    private static final String OPEN_WEBVIEW = "%s/app/show_order_detail/?user_id=%s&order_id=%s";


    @BindView(R.id.order_detail)
    WebView orderDetail;

    private Integer userId = 0;
    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);
        if(Utils.checkLogin()){
            userId = BaseApplication.getLoginInfo().getUserId();
        }
        Integer orderId = getIntent().getIntExtra("order_id",0);
        webViewConfig(orderDetail);
        orderDetail.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        });
        orderDetail.loadUrl(String.format(OPEN_WEBVIEW, GlobalDefine.BACKGROUND_MANAGER_DOMAIN, userId,orderId));
        orderDetail.addJavascriptInterface(this, "orderDetail");
    }

    private void webViewConfig(WebView webView) {
        //声明WebSettings子类
        WebSettings webSettings = webView.getSettings();

        //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        webSettings.setJavaScriptEnabled(true);
        // 若加载的 html 里有JS 在执行动画等操作，会造成资源浪费（CPU、电量）
        // 在 onStop 和 onResume 里分别把 setJavaScriptEnabled() 给设置成 false 和 true 即可

        //支持插件
        //webSettings.setPluginsEnabled(true);

        //设置自适应屏幕，两者合用
        webSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
        webSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小

        //缩放操作
        webSettings.setSupportZoom(false); //支持缩放，默认为true。是下面那个的前提。
        webSettings.setBuiltInZoomControls(false); //设置内置的缩放控件。若为false，则该WebView不可缩放
        webSettings.setDisplayZoomControls(true); //隐藏原生的缩放控件

        //其他细节操作
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT); //关闭webview中缓存
        webSettings.setAllowFileAccess(true); //设置可以访问文件
        webSettings.setLoadsImagesAutomatically(true); //支持自动加载图片
        webSettings.setDefaultTextEncodingName("utf-8");//设置编码格式

        //优先使用缓存:
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        //缓存模式如下：
        //LOAD_CACHE_ONLY: 不使用网络，只读取本地缓存数据
        //LOAD_DEFAULT: （默认）根据cache-control决定是否从网络上取数据。
        //LOAD_NO_CACHE: 不使用缓存，只从网络获取数据.
        //LOAD_CACHE_ELSE_NETWORK，只要本地有，无论是否过期，或者no-cache，都使用缓存中的数据
    }

    @JavascriptInterface
    public void finishActivity(){
        finish();
    }

    @JavascriptInterface
    public void openDetail(String product_id){
        new ProductInfo(CommonStringUtils.safeInteger(product_id)).execute((Void) null);
    }


    public class ProductInfo extends AsyncTask<Void, Void, Void> {
        public Integer productId;
        public ProductInfo(Integer productId) {
            this.productId = productId;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Integer userId;
            if(Utils.checkLogin()) {
                userId = BaseApplication.getLoginInfo().getUserId();
            }else{
                userId = 0;
            }
            Map<String, String> map = BackgroundManagerUtils.getProductById(userId,productId);
            ProductData productData = new ProductData(map);
            runOnUiThread(() -> {
                Intent intent = new Intent(OrderDetailActivity.this,ProductDetailActivity.class);
                intent.putExtra("product_data",productData);
                startActivity(intent);
            });
            return null;
        }
    }
}
