package com.sh.secondhand;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.gson.Gson;
import com.sh.utils.Http.OkHttpUtils;

import java.util.ArrayList;

import static com.sh.struts.GlobalDefine.BACKGROUND_MANAGER_DOMAIN;

public class ProductGenreChoiseActivity extends AppCompatActivity{
    private static final String TAG = "ProductGenreChoiseActivity";
    private static final String GET_PRODUCT_GENRES_URL = "%s/app/get_product_genres/";

    private Intent data;

    @BindView(R.id.pro_genres)
    ListView proGenres;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    boolean redirectGenre = false;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_genre_choise);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        data = getIntent();
        redirectGenre = data.getBooleanExtra("redirect",false);
        initListView();
    }

    public void initListView(){
        String url = String.format(GET_PRODUCT_GENRES_URL,BACKGROUND_MANAGER_DOMAIN);
        OkHttpUtils.get(url, new OkHttpUtils.ResultCallback<String>() {

            @SuppressLint("LongLogTag")
            @Override
            public void onSuccess(String response) {
                ArrayList<String> proGenreList;
                try{
                    proGenreList = new Gson().fromJson(response,ArrayList.class);
                }catch (Exception e){
                    proGenreList = new ArrayList<>();
                    Log.e(TAG,e.getMessage());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(ProductGenreChoiseActivity.this,android.R.layout.simple_expandable_list_item_1,proGenreList);
                proGenres.setAdapter(adapter);
                ArrayList<String> finalProGenreList = proGenreList;
                if(redirectGenre){
                    proGenres.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            String text = finalProGenreList.get(i);
                            Intent intent = new Intent(ProductGenreChoiseActivity.this,PoductForGenreActivity.class);
                            intent.putExtra("genre",text);
                            startActivity(intent);
                        }
                    });
                }else {
                    proGenres.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            String text = finalProGenreList.get(i);
                            setResult(RESULT_OK, data.putExtra("pro_genre", text));
                            finish();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Exception e) {
                Toast.makeText(ProductGenreChoiseActivity.this,"获取失败，请重试",Toast.LENGTH_SHORT);
            }
        });
    }
}
