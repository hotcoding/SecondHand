package com.sh.secondhand;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.sh.Application.BaseApplication;
import com.sh.struts.GlobalDefine;
import com.sh.utils.CommonStringUtils;
import com.sh.utils.Utils;

public class ShowAllOrdersActivity extends AppCompatActivity {
    private static final String OPEN_WEBVIEW = "%s/app/show_orders/?user_id=%s";


    @BindView(R.id.all_orders_view)
    WebView allOrdersView;

    private Integer userId = 0;

    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            //设置修改状态栏
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            //设置状态栏的颜色，和你的app主题或者标题栏颜色设置一致就ok了
            window.setStatusBarColor(getResources().getColor(R.color.status_lan_back));
        }
        setContentView(R.layout.activity_show_all_orders);
        ButterKnife.bind(this);
        if (Utils.checkLogin()) {
            userId = BaseApplication.getLoginInfo().getUserId();
        }
        webViewConfig(allOrdersView);
        allOrdersView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        });
        allOrdersView.loadUrl(String.format(OPEN_WEBVIEW, GlobalDefine.BACKGROUND_MANAGER_DOMAIN, userId));
        allOrdersView.addJavascriptInterface(this, "allOrders");
    }

    private void webViewConfig(WebView webView) {
        //声明WebSettings子类
        WebSettings webSettings = webView.getSettings();

        //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        webSettings.setJavaScriptEnabled(true);
        // 若加载的 html 里有JS 在执行动画等操作，会造成资源浪费（CPU、电量）
        // 在 onStop 和 onResume 里分别把 setJavaScriptEnabled() 给设置成 false 和 true 即可

        //支持插件
        //webSettings.setPluginsEnabled(true);

        //设置自适应屏幕，两者合用
        webSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
        webSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小

        //缩放操作
        webSettings.setSupportZoom(false); //支持缩放，默认为true。是下面那个的前提。
        webSettings.setBuiltInZoomControls(false); //设置内置的缩放控件。若为false，则该WebView不可缩放
        webSettings.setDisplayZoomControls(true); //隐藏原生的缩放控件

        //其他细节操作
        webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK); //关闭webview中缓存
        webSettings.setAllowFileAccess(true); //设置可以访问文件
        webSettings.setLoadsImagesAutomatically(true); //支持自动加载图片
        webSettings.setDefaultTextEncodingName("utf-8");//设置编码格式

        //优先使用缓存:
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        //缓存模式如下：
        //LOAD_CACHE_ONLY: 不使用网络，只读取本地缓存数据
        //LOAD_DEFAULT: （默认）根据cache-control决定是否从网络上取数据。
        //LOAD_NO_CACHE: 不使用缓存，只从网络获取数据.
        //LOAD_CACHE_ELSE_NETWORK，只要本地有，无论是否过期，或者no-cache，都使用缓存中的数据
    }

    @JavascriptInterface
    public void finishActivity() {
        finish();
    }

    @JavascriptInterface
    public void openDetail(String order_id) {
        Intent intent = new Intent(ShowAllOrdersActivity.this, OrderDetailActivity.class);
        intent.putExtra("order_id", CommonStringUtils.safeInteger(order_id));
        startActivity(intent);
    }

    @JavascriptInterface
    public void reloadWeb() {
        allOrdersView.post(() -> {
            allOrdersView.reload();
        });
    }
}
