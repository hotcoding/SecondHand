package com.sh.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.qiniu.android.utils.StringUtils;
import com.sh.Application.BaseApplication;
import com.sh.secondhand.ProductDetailActivity;
import com.sh.secondhand.R;
import com.sh.struts.LoginInfo;
import com.sh.struts.ProductData;
import com.sh.adapter.ProductWaterFallAdapter;
import com.sh.utils.BackgroundManagerUtils;
import com.sh.utils.CommonStringUtils;
import com.sh.utils.Http.OkHttpUtils;

import java.util.*;

public class ProductViewFragment extends Fragment {
    private static final String TAG = "ProductViewFragment";
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private String genreName;
    private String sort;
    private Integer distance = -1;
    private String title = "";
    private boolean isFreeExpress = false;
    private boolean isAllNew = false;
    private double minPrice = 0;
    private double maxPrice = 0;

    @BindView(R.id.product_show)
    RecyclerView productView;

    protected List<Integer> loadedProductIds;
    private StaggeredGridLayoutManager mLayoutManager;
    private ProductWaterFallAdapter mAdapter;
    private boolean canload = true; // 作为是否还有数据的标志
    private View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (null != rootView) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (null != parent) {
                parent.removeView(rootView);
            }
        } else {
            rootView = inflater.inflate(R.layout.products_fragment, container,false);
            ButterKnife.bind(this,rootView);
            initRecylerView();
        }
        return rootView;
    }

    public ProductViewFragment(){}

    @SuppressLint("ValidFragment")
    public ProductViewFragment(String genre){
        this.genreName = genre;
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ProductViewFragment newInstance(int sectionNumber,String genre) {
        ProductViewFragment fragment = new ProductViewFragment(genre);
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }


    public void initRecylerView(){
        productView.setNestedScrollingEnabled(false);
        mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mAdapter = new ProductWaterFallAdapter(BaseApplication.getInstance(), new ArrayList<>());
        productView.setLayoutManager(mLayoutManager);
        productView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(position -> {
            Intent webView = new Intent(this.getActivity(), ProductDetailActivity.class);
            webView.putExtra("product_data",mAdapter.getItemByPosition(position));
            startActivity(webView);
        });
        productView.addOnScrollListener(new ScrollListener() {
            @Override
            public void onLoadMore() {
                if (canload) {
                    mAdapter.setLoadState(mAdapter.LOADING);
                    new ProductInfo(loadedProductIds, 10,genreName,sort,distance).execute((Void) null);
                    mAdapter.setLoadState(mAdapter.LOADING_COMPLETE);
                } else {
                    mAdapter.setLoadState(mAdapter.LOADING_END);
                    Log.i(TAG,"can't loading");
                }
            }
        });
        loadedProductIds = new ArrayList<>();
        new ProductInfo(loadedProductIds, 10,genreName,sort,distance).execute((Void) null);
    }

    abstract class ScrollListener extends RecyclerView.OnScrollListener {
        // 用来标记是否正在向上滑动
        private boolean isSlidingUpward = false;

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            StaggeredGridLayoutManager manager = (StaggeredGridLayoutManager) recyclerView.getLayoutManager();
            // 当不滑动时
            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                // 获取最后一个完全显示的itemPosition
                if (!canScrollDown(recyclerView) && isSlidingUpward) {
                    // 加载更多
                    onLoadMore();
                }
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            // 大于0表示正在向上滑动，小于等于0表示停止或向下滑动
            isSlidingUpward = dy > 0;
            if (!canScrollDown(recyclerView) && isSlidingUpward) {
                // 加载更多
                onLoadMore();
            }
        }

        /**
         * 加载更多回调
         */
        public abstract void onLoadMore();
    }

    public void loadDataByFilter(String sort,Integer distance,String title,boolean isFreeExpress,boolean isAllNew,double minPrice,double maxPrice){
        this.sort = sort;
        this.distance = distance;
        this.title = StringUtils.isNullOrEmpty(title) ? null : title;
        this.isFreeExpress = isFreeExpress;
        this.isAllNew = isAllNew;
        this.minPrice = minPrice;
        this.maxPrice= maxPrice;
        loadedProductIds.clear();
        mAdapter.clearData();
        canload = true;
        mAdapter.setLoadState(mAdapter.LOADING);
        new ProductInfo(loadedProductIds, 10,genreName,sort,distance).execute((Void) null);
        mAdapter.setLoadState(mAdapter.LOADING_COMPLETE);
    }

    private boolean canScrollDown(RecyclerView recyclerView) {
        return ViewCompat.canScrollVertically(recyclerView, 1);
    }

    public class ProductInfo extends AsyncTask<Void, Void, Void> {
        List<Integer> productIds;
        Integer count;
        String genre;
        String sort;
        Integer distance;

        public ProductInfo(List<Integer> productIds, Integer count,String genre,String sort,Integer distance) {
            this.productIds = productIds;
            this.count = count;
            this.genre = genre;
            this.sort = sort;
            this.distance = distance;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            LoginInfo loginInfo = BaseApplication.getLoginInfo();
            Integer userId = loginInfo == null ? 0 : loginInfo.getUserId();
            List<OkHttpUtils.Param> params = new ArrayList<>();
            params.add(new OkHttpUtils.Param("poduct_ids", CommonStringUtils.join(productIds, ",")));
            if(!StringUtils.isNullOrEmpty(title)){
                params.add(new OkHttpUtils.Param("title",title));
            }
            params.add(new OkHttpUtils.Param("is_free_express",CommonStringUtils.safeString(isFreeExpress)));
            params.add(new OkHttpUtils.Param("is_all_new",CommonStringUtils.safeString(isAllNew)));
            if(minPrice != 0){
                params.add(new OkHttpUtils.Param("min_price",CommonStringUtils.safeString(minPrice)));
            }
            if(maxPrice != 0){
                params.add(new OkHttpUtils.Param("max_price",CommonStringUtils.safeString(maxPrice)));
            }
            if(userId != null && userId != 0){
                params.add(new OkHttpUtils.Param("user_id",CommonStringUtils.safeString(userId)));
            }
            if(!StringUtils.isNullOrEmpty(genre)){
                params.add(new OkHttpUtils.Param("genre",genre));
            }
            params.add(new OkHttpUtils.Param("count",CommonStringUtils.safeString(count)));
            if(!StringUtils.isNullOrEmpty(sort)) {
                params.add(new OkHttpUtils.Param("sort", sort));
            }
            if(distance != -1){
                params.add(new OkHttpUtils.Param("distance",CommonStringUtils.safeString(distance)));
            }
            List<Map<String, String>> products = BackgroundManagerUtils.getProducts(params);
            Set<Integer> productIdSet = new HashSet<>(loadedProductIds);
            if (products.size() < count) {
                canload = false;
            }
            List<ProductData> datas = new ArrayList<>();
            for (Map map : products) {
                if (productIdSet.add(CommonStringUtils.safeInteger(map.get("pro_id")))) {
                    ProductData productData = new ProductData(map);
                    datas.add(productData);
                }
            }
            getActivity().runOnUiThread(() -> {
                mAdapter.addItems(datas);
                if(!canload){
                    mAdapter.setLoadState(mAdapter.LOADING_END);
                    mAdapter.notifyDataSetChanged();
                }
            });
            loadedProductIds = new ArrayList<>(productIdSet);
            return null;
        }
    }

    /**
     * 在子线程里需要使用Toast
     *
     * @param context
     * @param message
     */
    public void showToastOnThread(Context context, String message) {
        getActivity().runOnUiThread(() -> Toast.makeText(context, message, Toast.LENGTH_SHORT).show());
    }
}
