package com.sh.asyntask;

import android.os.AsyncTask;
import com.sh.handler.LoginHandler;

public class AutoLoginTask extends AsyncTask {
    @Override
    protected Object doInBackground(Object[] objects) {
        LoginHandler.autoLogin();
        return null;
    }
}
