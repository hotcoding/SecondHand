package com.sh.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.sh.secondhand.R;
import com.sh.struts.ProductData;

import java.util.Collection;
import java.util.List;

public class ProductWaterFallAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<ProductData> dataList;
    // 普通布局
    private final int TYPE_ITEM = 1;
    // 脚布局
    private final int TYPE_FOOTER = 2;
    // 当前加载状态，默认为加载完成
    private int loadState = 2;
    // 正在加载
    public final int LOADING = 1;
    // 加载完成
    public final int LOADING_COMPLETE = 2;
    // 加载到底
    public final int LOADING_END = 3;
    public ProductWaterFallAdapter(Context context, List<ProductData> datas){
        this.context = context;
        this.dataList = datas;
    }

    @Override
    public int getItemViewType(int position) {
        // 最后一个item设置为FooterView
        if (position + 1 == getItemCount()) {
            return TYPE_FOOTER;
        } else {
            return TYPE_ITEM;
        }
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_item, null);
            return new MyViewHolder(view);
        } else if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(context)
                    .inflate(R.layout.layout_refresh_footer, parent, false);
            return new FootViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position) == TYPE_ITEM) {
            MyViewHolder holder2 = (MyViewHolder) holder;
            ProductData productData = dataList.get(position);
            String showPath = productData.img;
            String userImg = productData.userImg;
            String username = productData.username;
            String title = productData.title;
            double price = productData.price;
            Uri showImgUri = Uri.parse(showPath);
            Uri userImgUri = Uri.parse(userImg);
            holder2.showImg.setImageURI(showImgUri);
            holder2.userImg.setImageURI(userImgUri);
            holder2.title.setText(title);
            holder2.price.setText("¥" + price);
            holder2.userName.setText(username);

            // 点击事件一般都写在绑定数据这里，当然写到上边的创建布局时候也是可以的
            if (mItemClickListener != null) {
                holder2.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 这里利用回调来给RecyclerView设置点击事件
                        mItemClickListener.onItemClick(position);
                    }
                });
            }
        }else if(getItemViewType(position) == TYPE_FOOTER){
            FootViewHolder footViewHolder = (FootViewHolder) holder;
            switch (loadState) {
                case LOADING: // 正在加载
                    footViewHolder.pbLoading.setVisibility(View.VISIBLE);
                    footViewHolder.tvLoading.setVisibility(View.VISIBLE);
                    footViewHolder.llEnd.setVisibility(View.GONE);
                    break;

                case LOADING_COMPLETE: // 加载完成
                    footViewHolder.pbLoading.setVisibility(View.INVISIBLE);
                    footViewHolder.tvLoading.setVisibility(View.INVISIBLE);
                    footViewHolder.llEnd.setVisibility(View.GONE);
                    break;

                case LOADING_END: // 加载到底
                    footViewHolder.pbLoading.setVisibility(View.GONE);
                    footViewHolder.tvLoading.setVisibility(View.GONE);
                    footViewHolder.llEnd.setVisibility(View.VISIBLE);
                    break;

                default:
                    break;
            }
            ViewGroup.LayoutParams params = ((FootViewHolder) holder).llEnd.getLayoutParams();
            params.height = 400;
            ((FootViewHolder) holder).llEnd.setLayoutParams(params);
        }


        /*// 给RecyclerView中item中的单独控件设置点击事件 可以直接在adapter中使用setOnClickListener即可
        holder2.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });*/
    }

    @Override
    public int getItemCount() {
        if(dataList != null){
            return dataList.size() + 1;
        }
        return 0;
    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        ViewGroup.LayoutParams lp = holder.itemView.getLayoutParams();
        if(lp != null && lp instanceof StaggeredGridLayoutManager.LayoutParams) {
            StaggeredGridLayoutManager.LayoutParams p = (StaggeredGridLayoutManager.LayoutParams) lp;
            p.setFullSpan(holder.getItemViewType() == TYPE_FOOTER);
        }
    }

    /**
     * 设置上拉加载状态
     *
     * @param loadState 0.正在加载 1.加载完成 2.加载到底
     */
    public void setLoadState(int loadState) {
        if(this.loadState != loadState) {
            this.loadState = loadState;
            notifyDataSetChanged();
        }
    }

    //定义自己的ViewHolder，将View的控件引用在成员变量上
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public SimpleDraweeView showImg;
        public SimpleDraweeView userImg;
        public TextView userName;
        public TextView title;
        public TextView price;
        public CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            showImg = (SimpleDraweeView) itemView.findViewById(R.id.show_img);
            userImg = (SimpleDraweeView) itemView.findViewById(R.id.user_img);
            userName = (TextView) itemView.findViewById(R.id.user_name);
            title = (TextView)itemView.findViewById(R.id.title);
            price = (TextView)itemView.findViewById(R.id.price);
            cardView = (CardView)itemView.findViewById(R.id.pro_cardview);
        }
    }

    private class FootViewHolder extends RecyclerView.ViewHolder {

        ProgressBar pbLoading;
        TextView tvLoading;
        LinearLayout llEnd;

        FootViewHolder(View itemView) {
            super(itemView);
            pbLoading = (ProgressBar) itemView.findViewById(R.id.pb_loading);
            tvLoading = (TextView) itemView.findViewById(R.id.tv_loading);
            llEnd = (LinearLayout) itemView.findViewById(R.id.ll_end);
        }
    }

    private ItemClickListener mItemClickListener ;
    public interface ItemClickListener{
        public void onItemClick(int position) ;
    }
    public void setOnItemClickListener(ItemClickListener itemClickListener){
        this.mItemClickListener = itemClickListener ;

    }

    public void addItem(ProductData data){
        this.dataList.add(data);
        notifyDataSetChanged();
    }

    public void addItems(Collection< ? extends ProductData> datas){
        this.dataList.addAll(datas);
        notifyDataSetChanged();
    }

    public ProductData getItemByPosition(int position){
        if(dataList.size() > position){
            return dataList.get(position);
        }
        return null;
    }

    public void clearData(){
        dataList.clear();
        notifyDataSetChanged();
    }
}
