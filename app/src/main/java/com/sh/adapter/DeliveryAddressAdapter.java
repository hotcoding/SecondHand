package com.sh.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.sh.secondhand.R;
import com.sh.struts.DeliveryAddressData;

import java.util.Collection;
import java.util.List;

public class DeliveryAddressAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "DeliveryAddressAdapter";
    private Context context;
    private List<DeliveryAddressData> dataList;
    // 普通布局
    private final int TYPE_ITEM = 1;
    // 脚布局
    private final int TYPE_FOOTER = 2;

    public DeliveryAddressAdapter(Context context, List<DeliveryAddressData> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getItemViewType(int position) {
        // 最后一个item设置为FooterView
        if (position + 1 == getItemCount()) {
            return TYPE_FOOTER;
        } else {
            return TYPE_ITEM;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.delivery_address_item, parent, false);
            return new MyViewHolder(view);
        } else if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(context)
                    .inflate(R.layout.delivery_address_footer, parent, false);
            return new FootViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_ITEM) {
            MyViewHolder holder2 = (MyViewHolder) holder;
            DeliveryAddressData deliveryAddressData = dataList.get(position);
            holder2.nameArea.setText(String.format("收货人： %s", deliveryAddressData.name));
            holder2.mobileArea.setText(deliveryAddressData.mobile);
            holder2.fullAddressArea.setText(String.format("收货地址： %s", deliveryAddressData.getRealFullAddress()));

            // 点击事件一般都写在绑定数据这里，当然写到上边的创建布局时候也是可以的
            if (mItemClickListener != null) {
                holder2.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 这里利用回调来给RecyclerView设置点击事件
                        mItemClickListener.onItemClick(position,v);
                    }
                });
            }
        } else if (getItemViewType(position) == TYPE_FOOTER) {
            FootViewHolder footViewHolder = (FootViewHolder) holder;
            if(btnClickListener != null){
                footViewHolder.addDeliveryAddress.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        btnClickListener.buttonClick();
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        if (dataList != null) {
            return dataList.size() + 1;
        }
        return 0;
    }

    //定义自己的ViewHolder，将View的控件引用在成员变量上
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nameArea;
        public TextView mobileArea;
        public TextView fullAddressArea;

        public MyViewHolder(View itemView) {
            super(itemView);
            nameArea = itemView.findViewById(R.id.name_area);
            mobileArea = itemView.findViewById(R.id.mobile_area);
            fullAddressArea = itemView.findViewById(R.id.full_address_area);
        }
    }

    private class FootViewHolder extends RecyclerView.ViewHolder {
        public Button addDeliveryAddress;

        FootViewHolder(View itemView) {
            super(itemView);
            addDeliveryAddress = itemView.findViewById(R.id.btn_add_delivery_address);
        }
    }

    private ItemClickListener mItemClickListener;

    public interface ItemClickListener {
        public void onItemClick(int position,View view);
    }

    public void setOnItemClickListener(ItemClickListener itemClickListener) {
        this.mItemClickListener = itemClickListener;
    }

    private ButtonInterface btnClickListener;

    public interface ButtonInterface {
        public void buttonClick();
    }

    public void buttonSetOnclick(ButtonInterface buttonInterface) {
        this.btnClickListener = buttonInterface;
    }

    public void addItems(Collection< ? extends DeliveryAddressData> datas){
        this.dataList.addAll(datas);
        notifyDataSetChanged();
    }

    public DeliveryAddressData getItemByPosition(int position){
        if(dataList.size() > position){
            return dataList.get(position);
        }
        return null;
    }

    public void clearData(){
        dataList.clear();
        notifyDataSetChanged();
    }
}
