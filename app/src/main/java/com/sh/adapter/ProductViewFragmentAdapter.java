package com.sh.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.sh.fragment.ProductViewFragment;

import java.util.List;

public class ProductViewFragmentAdapter extends FragmentPagerAdapter {
    private FragmentManager fragmetnmanager;  //创建FragmentManager
    private List<ProductViewFragment> listfragment; //创建一个List<Fragment>

    public ProductViewFragmentAdapter(FragmentManager fm, List<ProductViewFragment> list) {
        super(fm);
        this.fragmetnmanager = fm;
        this.listfragment = list;
    }

    @Override
    public Fragment getItem(int position) {
        return listfragment.get(position);
    }

    @Override
    public int getCount() {
        return listfragment == null ? 0 : listfragment.size();
    }
}
