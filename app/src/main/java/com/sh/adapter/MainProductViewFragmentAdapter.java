package com.sh.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.sh.fragment.MainProductViewFrament;

import java.util.List;

public class MainProductViewFragmentAdapter extends FragmentPagerAdapter {
    private FragmentManager fragmetnmanager;  //创建FragmentManager
    private List<MainProductViewFrament> listfragment; //创建一个List<Fragment>

    public MainProductViewFragmentAdapter(FragmentManager fm, List<MainProductViewFrament> list) {
        super(fm);
        this.fragmetnmanager = fm;
        this.listfragment = list;
    }

    @Override
    public Fragment getItem(int position) {
        return listfragment.get(position);
    }

    @Override
    public int getCount() {
        return listfragment == null ? 0 : listfragment.size();
    }
}
